# Gridview Inflater Module

A configurable and customizable GridView Inflater module for android.



## Installation

From Android Studio, import the library module to your project :

  1. Click File > New > Import Module

  2. Choose **gridview-inflater-module** directory and click Finish. 
     *In case of any import error, just proceed to step 3 and the error should be automatically solved.*

  3. Make sure the library is listed at the top of your **settings.gradle file**. If not, add this line :

	```
	include ":app", ":gridview-inflater-module"   
	```

  4. Open the **app gradle file** and add a new line to the dependencies block as shown in the following snippet :

	```
	dependencies {
		implementation project(":gridview-inflater-module")
    	implementation "com.android.support:recyclerview-YOUR_VERSION_OF_SUPPORT_APPCOMPAT"
	}
	```

  5. Click Sync Project with Gradle Files (Sync Now)

*Note that you must change YOUR_VERSION_OF_SUPPORT_APPCOMPAT that you will find a few lines before in the same file. In my case, I had : implementation "com.android.support:recyclerview-v7:28.0.0-rc01"*



## Usage

This module aims to inflate a LinearLayout contains into a ScrollView in your activity .xml layout.

In the activity in which you want to call the module, import it by adding the following line :

```java
import com.mecreativestudio.gridview_inflater_module.GridViewInflater;
```



Then you can simply use it by inflating the LinearLayout, as follows :

```java
//Find the linearLayout and inflate it with the gridView
final LinearLayout YOUR_LINEAR_LAYOUT = findViewById(R.id.YOUR_LINEAR_LAYOUT_ID);

LayoutInflater.from(this).inflate(com.mecreativestudio.gridview_inflater_module.R.layout.activity_grid_view, YOUR_LINEAR_LAYOUT);


/*---------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------HANDLE THE SCROLLING BEHAVIOR-----------------------------------------*/
/*----------------------------YOU JUST NEED TO CHANGE THE UPPERCASE VARIABLES NAMES------------------------------*/
/*---------------------------------------------------------------------------------------------------------------*/

//Get the vertical position of the linearLayout
YOUR_LINEAR_LAYOUT.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(){
	@Override public void onGlobalLayout(){
		int [] location = new int[2];
		YOUR_LINEAR_LAYOUT.getLocationOnScreen(location);
		linearLayoutPosition = location[1];
		YOUR_LINEAR_LAYOUT.getViewTreeObserver().removeGlobalOnLayoutListener(this);
 	}
 });

//YOU DO NOT NEED TO MODIFY THIS PART
//Find the recyclerView of the GridView and disabled its own scrolling
final RecyclerView recyclerView = findViewById(com.mecreativestudio.gridview_inflater_module.R.id.recyclerView);
recyclerView.setNestedScrollingEnabled(false);

//Find the scrollView of the activity and adding an onScrollListener on it
final ScrollView YOUR_SCROLL_VIEW = findViewById(R.id.YOUR_SCROLL_VIEW_ID);
YOUR_SCROLL_VIEW.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
	@Override
	public void onScrollChanged() {
		//Get the current position of the vertical scroll
		int scrollY = YOUR_SCROLL_VIEW.getScrollY();
		//If the vertical scroll is equal or greater than the linear layout vertical position (means that the linearLayout is on the top of the screen)
		if(scrollY >= linearLayoutPosition){
 			//Enabled the scrolling inside the recycler view
 			recyclerView.setNestedScrollingEnabled(true);
			}else{
				//Else, disabled it again
				recyclerView.setNestedScrollingEnabled(false);
			}
    	}
	}
);

/*---------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------*/


new GridViewInflater(
    YOUR_API_URL (STRING), 
    IS_PAGINATED (BOOLEAN), 
    PAGINATION_PARAMETER (STRING), 
    PAGINATION_PARAMETER_VALUE (INT), 
    SHOULD_PAGINATION_PARAMTER_AUTO_INCREMENT (BOOLEAN), 
    YOUR_ARRAY_LIST_OF_URL_QUERIES (STRINGS), 
    NUMBER_OF_ITEMS_PER_ROW (INT), 
    MAXIMUM_NUMBER_OF_ITEMS_PER_REQUEST (INT), 
    recyclerView
);
```

*Note that you must change YOUR_LINEAR_LAYOUT, YOUR_LINEAR_LAYOUT_ID, YOUR_SCROLL_VIEW and YOUR_SCROLL_VIEW_ID by their respective names.*
*Of course, you must also change the parameters passed to the GridViewInflater class instance (except recyclerView)*



### 	Example

Let's say you want to inflate a LinearLayout, contained in a ScrollView, with a gridView populated with the data sent by this API ( https://reqres.in/).

This API returns a list of users ( avatars, first names and surnames) but you only want to display the avatars.
Having read the documentation, you know that the data is paginated and you know what queries you want to make.

In your application, you have an activity called ***MainActivity*** and its associated layout contains a ScrollView whose id is ***myScrollView*** and a LinearLayout whose id is ***myLinearLayout***. You want this LinearLayout to be inflated with the gridView.



Your MainActivity layout (*activity_main.xml*) should look like this :

```xml
<?xml version="1.0" encoding="utf-8"?>
<ScrollView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity"
    android:id="@+id/myScrollView">

    <android.support.constraint.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <LinearLayout
            android:id="@+id/myLinearLayout"
            android:layout_width="0dp"
            android:layout_height="400dp"
            android:layout_marginEnd="8dp"
            android:layout_marginLeft="8dp"
            android:layout_marginRight="8dp"
            android:layout_marginStart="8dp"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            android:orientation="horizontal" />
        
    </android.support.constraint.ConstraintLayout>

</ScrollView>
```



By following the previous instructions, the code which you should have in your MainActivity.java class is :

```JAVA
import ...
import com.mecreativestudio.gridview_inflater_module.GridViewInflater;

public class MainActivity extends AppCompatActivity {

    //IMPORTANT
    int linearLayoutPosition;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final LinearLayout myLinearLayout = findViewById(R.id.myLinearLayout);
        LayoutInflater.from(this).inflate(com.mecreativestudio.gridview_inflater_module.R.layout.activity_grid_view, myLinearLayout);

/*---------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------HANDLE THE SCROLLING BEHAVIOR-----------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------*/
        
        
        myLinearLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(){
            @Override public void onGlobalLayout(){
                int [] location = new int[2];
                myLinearLayout.getLocationOnScreen(location);
                linearLayoutPosition = location[1];
                myLinearLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);

            }
        });

        final RecyclerView recyclerView = findViewById(com.mecreativestudio.gridview_inflater_module.R.id.recyclerView);
        recyclerView.setNestedScrollingEnabled(false);

        final ScrollView myScrollView = findViewById(R.id.myScrollView);
        myScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = myScrollView.getScrollY();
                
                if(scrollY >= linearLayoutPosition){
                    recyclerView.setNestedScrollingEnabled(true);
                }else{
                    recyclerView.setNestedScrollingEnabled(false);
                }
            }
        });
        
/*---------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------*/
        

        ArrayList<String> urlQueries = new ArrayList<>();
        urlQueries.add("per_page=8");

        new GridViewInflater(
            "https://reqres.in/api/users", 
            true, 
            "page", 
            1, 
            true, 
            urlQueries, 
            4, 
            8, 
            recyclerView
        );

    }
}

```



#### Explanations

After consulting the documentation, we know that to retrieve the users list, the URL API is: https://reqres.in/api/users

According to the documentation, we want to get 8 users avatars per page ( *per_page = 8* ).

The data being paginated, the variable *isPaginated* is thus equal to *true*. 

According to the documentation, the *paginationParameter* is *page*. 
In this case, we want to start at page 1 ( *paginationParameterValue = 1*) and want to scan all API pages ( *shouldPaginationParameterAutoIncrement = true* ).

We also want to display 3 items / users avatars per row ( *numberOfItemsPerRow = 3* )

We want to make a request to the next page when we have retrieved the 8 users avatars of the current page. Therefore, we set the maximum number of items per request to 8 ( *maximumNumberOfItemsPerRequest = 8* ). 

Thus, when 8 users avatars have been retrieved, a request to the next page will be made.



## Configuration and Customization

This GridView Inflater Module is fully configurable and customizable.  

If you want to adapt its behavior to your needs, you can simply edit the GridViewInflater and Adapter Java classes which are located here :

```
YOUR_PROJECT/gridview-module/java/com.mecreativestudio.gridview_inflater_module/GridViewInflater.java

YOUR_PROJECT/gridview-module/java/com.mecreativestudio.gridview_inflater_module/Adapter.java
```



#### Guideline	

1.   In the GridViewInflater Java Class : 

      - Adapt the JSON Array (line 173) to your case
      - Adapt the Item class
      - Adapt the initArrayLists method
      - Adapt the populateItem method
      - Adapt the adapter (line 253)



2.   In the Adapter Java Class :

	 - Adapt the variables (line 20) to your case

	 - Adapt the 4 methods below the line 58



*Important : These files have been optimized for case-by-case use. It will therefore not be necessary for you to modify the parts of the code not mentioned above.*



If you want to edit its layout, you can simply edit the activity_recycler_view.xml file which is located here :

```xml
YOUR_PROJECT/gridview-inflater-module/res/layout/activity_grid_view.xml
```



If you want to edit the layout of a single item, you can simply edit the item.xml file which is located here :

```
YOUR_PROJECT/gridview-inflater-module/res/layout/item.xml
```

*Note that in this case, it is also essential to modify the resizeImages method (at line 45) of the Adapter.java class mentioned above.*