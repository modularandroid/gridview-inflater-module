package com.mecreativestudio.gridview_inflater_module;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    //TODO : STEP 1 : ADAPT TO YOUR NEED THE VARIABLES BELOW
    private ArrayList<String> id;
    private ArrayList<String> title;
    private ArrayList<String> imgURL;
    private ArrayList<String> description;


    /*--------------------------------------------------------------------------------------------------------------*/
    /*---------------------------------YOU PROBABLY DO NOT HAVE TO MODIFY THIS PART---------------------------------*/
    /*----------------------------------------------------START-----------------------------------------------------*/
    View view;
    private Context mContext;
    private static final String TAG = "Adapter";
    private int numberOfItemsPerRow;
    private int screenWidth;
    private int avalaibleScreenWidth;
    private int imagesTotalWidth;
    private int imageWidth;
    private int imageHeight;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }

    private void resizeImages(){
        screenWidth = mContext.getResources().getDisplayMetrics().widthPixels;
        avalaibleScreenWidth = screenWidth - 10;
        imagesTotalWidth = avalaibleScreenWidth - (numberOfItemsPerRow - 1)*5;
        imageWidth = imagesTotalWidth / numberOfItemsPerRow;
        imageHeight = imageWidth;
    }
    /*-----------------------------------------------------END------------------------------------------------------*/
    /*--------------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------------------------------------------------------------------------*/


    //TODO : STEP 2 : ADAPT TO YOUR NEED THE 4 METHODS BELOW
    @Override
    public int getItemCount() {
        return title.size();
    }

    public Adapter(int numberOfItemsPerRow, ArrayList<String> id, ArrayList<String> title, ArrayList<String> imgURL, ArrayList<String> description, Context mContext) {
        this.numberOfItemsPerRow = numberOfItemsPerRow;
        this.id = id;
        this.title = title;
        this.imgURL = imgURL;
        this.description = description;
        this.mContext = mContext;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.color.gray)
                .fitCenter()
                .centerCrop();

        resizeImages();
        holder.userImg.getLayoutParams().height = imageHeight;
        holder.userImg.getLayoutParams().width = imageWidth;
        holder.userImg.requestLayout();

        Glide.with(mContext)
                .load(imgURL.get(position))
                .apply(requestOptions)
                .into(holder.userImg);

        //You can add Listeners to any view. By default, only the constraintLayout has listener
        holder.constaintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO: Instructions to execute when clicking on an item
                String dataToDisplpay = title.get(position) + " " + description.get(position);
                Log.i("USER INFO", dataToDisplpay);
            }
        });

    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ConstraintLayout constaintLayout;
        private ImageView userImg;

        public ViewHolder(View itemView) {
            super(itemView);
            this.userImg = itemView.findViewById(R.id.userImg);
            this.constaintLayout = itemView.findViewById(R.id.constraintLayout);
        }
    }
}
