package com.mecreativestudio.gridview_inflater_module;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.github.pwittchen.infinitescroll.library.InfiniteScrollListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import okhttp3.OkHttpClient;

public class GridViewInflater extends AppCompatActivity {

    /*--------------------------------------------------------------------------------------------------------------*/
    /*---------------------------------YOU PROBABLY DO NOT HAVE TO MODIFY THIS PART---------------------------------*/
    /*----------------------------------------------------START-----------------------------------------------------*/
    private GridLayoutManager gridLayoutManager;
    private RecyclerView recyclerView;
    private Adapter adapter;
    private boolean isFirstLoad = true;
    private boolean isEndReached = false;
    private String next;
    private String urlQuery = "";
    private int itemsAlreadyLoaded = 0;

    private String apiUrl;
    private ArrayList<String> urlQueries;
    private boolean isPaginated;
    private String paginationParameter;
    private int paginationParameterValue;
    private boolean shouldPaginationParameterAutoIncrement;
    private int numberOfItemsPerRow;
    private int maximumNumberOfItemsPerRequest;

    Item item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);
    }

    public GridViewInflater(String apiUrl, boolean isPaginated, String paginationParameter, int paginationParameterValue, boolean shouldPaginationParameterAutoIncrement,  ArrayList<String> urlQueries, int numberOfItemsPerRow, int maximumNumberOfItemsPerRequest, RecyclerView recyclerView){
        this.apiUrl = apiUrl;
        this.isPaginated = isPaginated;
        this.paginationParameter = "?" + paginationParameter + "=";
        this.paginationParameterValue = paginationParameterValue;
        this.shouldPaginationParameterAutoIncrement = shouldPaginationParameterAutoIncrement;

        this.urlQueries = urlQueries;
        for(int i = 0; i < this.urlQueries.size(); i++){
            urlQuery += "?" + this.urlQueries.get(i);
        }

        this.numberOfItemsPerRow = numberOfItemsPerRow;
        this.maximumNumberOfItemsPerRequest = maximumNumberOfItemsPerRequest;

        item = new Item();
        initArrayLists();
        initRecyclerView(recyclerView);
    }

    @NonNull
    private InfiniteScrollListener createInfiniteScrollListener() {

        //If it is the first load for the Recycler View
        if(isFirstLoad){
            //If the data are paginated
            if(isPaginated){
                //Load the data with the pagination parameter and its value
                getItemsToBeLoaded(apiUrl + urlQuery + paginationParameter + paginationParameterValue);

                //If the pagination parameter should auto increment
                if(shouldPaginationParameterAutoIncrement){
                    paginationParameterValue++;
                }
            }else{
                //If the data are note paginated, load the data of the API URL
                getItemsToBeLoaded(apiUrl + urlQuery);
            }
            //Then, it is not the first load anymore
            isFirstLoad = false;
        }

        return new InfiniteScrollListener(maximumNumberOfItemsPerRequest, gridLayoutManager) {

                @Override
            public void onScrolledToEnd(final int firstVisibleItemPosition) {
                if (!isEndReached) {
                    String newURL;

                    if(isPaginated){
                        if(shouldPaginationParameterAutoIncrement){
                            newURL = apiUrl +  urlQuery + paginationParameter + next;
                        }else{
                            newURL = apiUrl +  urlQuery + paginationParameter + paginationParameterValue;
                        }
                        getItemsToBeLoaded(newURL);
                    }else{
                        getItemsToBeLoaded(apiUrl + urlQuery);
                    }
                }
            }
        };
    }

    private void getItemsToBeLoaded(String URL) {
        Log.i("URL",URL);
        getData(URL);
    }

    private void getData(final String URL) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    OkHttpClient client = new OkHttpClient();

                    okhttp3.Request request;
                    request = new okhttp3.Request.Builder()
                            .header("Content-Type", "content-type/application/json;")
                            .url(URL)
                            .get()
                            .build();

                    final okhttp3.Response response = client.newCall(request).execute();
                    if (response.isSuccessful()) {

                        runOnUiThread( new Runnable(){
                            @Override

                            public void run() {
                                try {
                                    setPostCategoryData((new JSONObject(response.body().string())));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                //If the recycler view does not contain data, init it
                                if(recyclerView.getChildCount() == 0){
                                    initRecyclerView(recyclerView);
                                //Otherwise, notify of data set changed
                                }else{
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        });
                    }else{
                        //Handle error
                    }
                }catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
    /*-----------------------------------------------------END------------------------------------------------------*/
    /*--------------------------------------------------------------------------------------------------------------*/
    /*--------------------------------------------------------------------------------------------------------------*/


    //TODO: STEP 1 : ADAPT THE JSON ARRAY (LINE 173) TO YOUR NEED
    private void setPostCategoryData(JSONObject response){
        try {
            //Log.i("RESPONSE",response.toString());
            JSONArray itemsJSONArray = response.getJSONArray("data");

            //If the wanted number of items per request is greater than the number of items in the response itself
            if(maximumNumberOfItemsPerRequest > itemsJSONArray.length() && itemsJSONArray.length() > 0){
                //Redefined the wanted number of items per request
                maximumNumberOfItemsPerRequest = itemsJSONArray.length();
            }

            //If the data are not paginated
            if(!isPaginated) {
                //If the number of items already displayed is equal or greater than (for security) the number of items in the API itself
                if (itemsAlreadyLoaded >= itemsJSONArray.length()) {
                    isEndReached = true;
                    //Otherwise collect the remaining data
                }else {
                    for (int i = itemsAlreadyLoaded; i < itemsJSONArray.length(); i++) {
                        JSONObject itemData = (JSONObject) itemsJSONArray.get(i);
                        populateItem(itemData);
                        itemsAlreadyLoaded++;
                    }
                }
                //The previous verification is unnecessary here has the pagination makes already the job
            }else{
                for (int i = 0; i < itemsJSONArray.length(); i++) {
                    JSONObject itemData = (JSONObject) itemsJSONArray.get(i);
                    populateItem(itemData);
                }
            }

            if(itemsJSONArray.length()!=0) {
                if(shouldPaginationParameterAutoIncrement){
                    next = String.valueOf(paginationParameterValue++);
                }
            }else{
                isEndReached = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    //TODO: STEP 2 : ADAPT THE ITEM CLASS TO YOUR NEED
    private class Item{
        private ArrayList<String> id;
        private ArrayList<String> title;
        private ArrayList<String> imgURL;
        private ArrayList<String> description;
    }

    //TODO: STEP 3 : ADAPT THE INIT ARRAY LISTS METHOD TO YOUR NEED
    private void initArrayLists() {
        item.id = new ArrayList<>();
        item.title = new ArrayList<>();
        item.imgURL = new ArrayList<>();
        item.description = new ArrayList<>();
    }

    //TODO: STEP 4 : ADAPT THE POPULATE ITEM METHOD TO YOUR NEED
    private void populateItem(JSONObject itemData){
        try {

            item.id.add(String.valueOf(itemData.get("id")));
            item.title.add(String.valueOf(itemData.get("first_name")));
            item.imgURL.add(String.valueOf(itemData.get("avatar")));
            item.description.add(String.valueOf(itemData.get("last_name")));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //TODO: STEP 5 : ADAPT THE ADAPTER (LINE 253) TO YOUR NEED
    private void initRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        gridLayoutManager = new GridLayoutManager(this,numberOfItemsPerRow);
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(gridLayoutManager);

        adapter = new Adapter(numberOfItemsPerRow,item.id, item.title, item.imgURL, item.description, recyclerView.getContext());

        this.recyclerView.setAdapter(adapter);
        this.recyclerView.addOnScrollListener(createInfiniteScrollListener());
    }
}
